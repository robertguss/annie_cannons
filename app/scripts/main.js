$(function() {

  // NAV OVERLAY SCRIPTS
  $('#nav-overlay').hide();

  $('.jquery-open').on('click', function(e) {
      e.preventDefault();
    $('#nav-overlay').fadeIn(1000);
    //$('.hero-logo-jquery, .hero-h2-jquery').fadeTo(1000, 0);
  });

  $('.nav-overlay-container').on('click', function() {
    $('#nav-overlay').fadeOut(1000);
    //$('.hero-logo-jquery, .hero-h2-jquery').fadeTo(1000, 1);
  });

// SHRINKING NAV SCRIPTS
  // $('.main-nav-container').data('size','big').css('background', 'rgba(255,255,255,0');
  $('.home-link, .ion-navicon, .top-nav-links').data('size','big').css('color', '#fff');
  $('.main-nav-container').data('size','big').css('border-bottom', '1px solid #fff');

  $(window).scroll(function(){
    if($(document).scrollTop() > 0)
    {
      if($('.main-nav-container').data('size') === 'big')
      {
          // $('.main-nav-container').data('size','small').css('background', 'rgba(255,255,255,1');
          $('.main-nav-container').toggleClass('main-nav-container-hover');
          $('#logo, .home-link, .ion-navicon, .top-nav-links').data('size','small').css('color', '#444');
          $('.main-nav-container').data('size','small').css('border-bottom', '1px solid #444');
          // MISSION TEXT & PLAY BTN
          $('.mission-header').toggleClass('mission-header-hover');
          $('.mission-copy').toggleClass('mission-copy-hover');
          $('.icon-music_play_button').toggleClass('icon-music_play_button-hover');
          $('.donate-link-nav').toggleClass('donate-link-nav-hover');
          $('.main-nav-container').stop().animate({
              height:'50px'
          },600);
      }
    }
    else
      {
        if($('.main-nav-container').data('size') === 'small')
          {
            // $('.main-nav-container').data('size','big').css('background', 'rgba(255,255,255,0');
            $('.main-nav-container').toggleClass('main-nav-container-hover');
            $('.home-link, .ion-navicon, .top-nav-links').data('size','big').css('color', '#fff');
            $('.main-nav-container').data('size','big').css('border-bottom', '1px solid #fff');
            // MISSION TEXT & PLAY BTN
            $('.mission-header').toggleClass('mission-header-hover');
            $('.mission-copy').toggleClass('mission-copy-hover');
            $('.icon-music_play_button').toggleClass('icon-music_play_button-hover');
            $('.donate-link-nav').toggleClass('donate-link-nav-hover');
            $('.main-nav-container').stop().animate({
                height:'60px'
            },600);
          }
      }
  });


//AUTO-SCROLL SCRIPTS
  $(function() {
    $('a.page-scroll').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
          scrollTop: $($anchor.attr('href')).offset().top
      }, 2500, 'easeInOutExpo');
      event.preventDefault();
    });
  });

// VIDEO HERO SCRIPTS
  // $('#mission').hide();


// VIDEO MODAL SCRIPTS
  $('#video-modal-section').hide();

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('#video-modal-section').fadeIn(1000);
  });

  $('.video-modal-container').on('click', function() {
    $('#video-modal-section').fadeOut(1000);
  });



// GET INVOLVED SECTION

  // TRAFFICKING CONTAINER
  $('.trafficking-container').on('mouseenter', function(event) {
    $('.trafficking-container').toggleClass('trafficking-container-hover');
    $('.trafficking-title').toggleClass('trafficking-title-hover');
  }).on('mouseleave', function(event) {
    $('.trafficking-container').toggleClass('trafficking-container-hover');
    $('.trafficking-title').toggleClass('trafficking-title-hover');
  });

  // DONATE CONTAINER
  $('.donate-container').on('mouseenter', function(event) {
    $('.donate-container').toggleClass('donate-container-hover');
    $('.donate-title').toggleClass('donate-title-hover');
  }).on('mouseleave', function(event) {
    $('.donate-container').toggleClass('donate-container-hover');
    $('.donate-title').toggleClass('donate-title-hover');
  });

  // VOLUNTEER CONTAINER
  $('.volunteer-container').on('mouseenter', function(event) {
    $('.volunteer-container').toggleClass('volunteer-container-hover');
    $('.volunteer-title').toggleClass('volunteer-title-hover');
  }).on('mouseleave', function(event) {
    $('.volunteer-container').toggleClass('volunteer-container-hover');
    $('.volunteer-title').toggleClass('volunteer-title-hover');
  });

  // HIRE CONTAINER
  $('.hire-container').on('mouseenter', function(event) {
    $('.hire-container').toggleClass('hire-container-hover');
    $('.hire-title').toggleClass('hire-title-hover');
  }).on('mouseleave', function(event) {
    $('.hire-container').toggleClass('hire-container-hover');
    $('.hire-title').toggleClass('hire-title-hover');
  });

  // CLIENTS TESTIMONIALS SLIDES

    $('.student-testimonials-testimonials').slick({
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 4000,
      fade: true,
      cssEase: 'linear'
    });


// TEAM SECTION
  (function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio
    $('#js-grid-lightbox-gallery').cubeportfolio({
        filters: '#js-filters-lightbox-gallery1, #js-filters-lightbox-gallery2',
        loadMore: '#js-loadMore-lightbox-gallery',
        loadMoreAction: 'click',
        layoutMode: 'grid',
        mediaQueries: [{
            width: 1500,
            cols: 6
        }, {
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 3
        }, {
            width: 480,
            cols: 2
        }, {
            width: 320,
            cols: 1
        }],
        defaultFilter: '*',
        animationType: 'rotateSides',
        gapHorizontal: 10,
        gapVertical: 10,
        gridAdjustment: 'responsive',
        caption: 'zoom',
        displayType: 'sequentially',
        displayTypeSpeed: 100,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePageInline
        singlePageInlineDelegate: '.cbp-singlePageInline',
        singlePageInlinePosition: 'below',
        singlePageInlineInFocus: true,
        singlePageInlineCallback: function(url, element) {
            // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 30000
                })
                .done(function(result) {

                    t.updateSinglePageInline(result);

                })
                .fail(function() {
                    t.updateSinglePageInline('AJAX Error! Please refresh the page!');
                });
        },
    });
  })(jQuery, window, document);

// HOME PAGE SLIDE SHOW
  $('.home-page-slideshow').slick({
    dots: true,
    infinite: true,
    speed: 2000,
    autoplay: true,
    autoplaySpeed: 5000,
    fade: true,
    adaptiveHeight: false,
    pauseOnHover: false,
    cssEase: 'linear'
  });


// PARTNERS CAROUSEL ON HOMEPAGE
  $(".owl-carousel").owlCarousel({
    items:4,
    loop:true,
    margin:30,
    autoplay:true,
    autoplayTimeout:2000
  });


// CURRICULUM TIMELINE SCRIPTS
  $.timeliner({});


}); // END
