const gulp                = require('gulp');
const uglify              = require('gulp-uglify');
const concat              = require('gulp-concat');
const sass                = require('gulp-sass');
const nano                = require('gulp-cssnano');
const jshint              = require('gulp-jshint');
const del                 = require('del');
const bsync               = require('browser-sync');
const reload              = bsync.reload;
const cached              = require('gulp-cached');
const remember            = require('gulp-remember');
const newer               = require('gulp-newer');
const sourcemaps          = require('gulp-sourcemaps');
const imagemin            = require('gulp-imagemin');
const postcss             = require('gulp-postcss');
const cssnext             = require('postcss-cssnext');
const rucksack            = require('gulp-rucksack');
const lost                = require('lost');
const plumber             = require('gulp-plumber');
const notify              = require('gulp-notify');
const nunjucksRender      = require('gulp-nunjucks-render');
const csslint             = require('gulp-csslint');
const reporters           = require('reporters');
const pngquant            = require('imagemin-pngquant');

// CLEANS DIST FOLDER
gulp.task('clean', () => {
  return del('dist/**/*');
});

// JS LINTING & HINTS
gulp.task('lint', () => {
  return gulp.src(
    ['app/scripts/**/*.js', '!app/scripts/vendor/**/*.js'], {
      since: gulp.lastRun('lint')
    })
    .pipe(jshint())
    //.pipe(jshint.reporter('default'))
    //.pipe(jshint.reporter('fail'));
    // Use gulp-notify as jshint reporter
    .pipe(notify(function (file) {
      if (file.jshint.success) {
        // Don't show something if success
        return false;
      }

      var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
      }).join("\n");
      return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
    }));
});

// SCRIPTS
gulp.task('scripts', gulp.series('lint', () => {
  return gulp.src(['app/scripts/**/*.js', '!app/scripts/vendor/*.js' ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(cached('scripts'))
    .pipe(uglify())
    .pipe(remember('scripts'))
    .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/scripts'))
    .pipe(bsync.stream());
}));

//MOVE VENDOR SCRIPTS TO DIST FOLDER
gulp.task('vendorScripts', () => {
  return gulp.src('app/scripts/vendor/**/*')
  .pipe(gulp.dest('dist/scripts/vendor'));
});

// STYLES
gulp.task('styles', () => {

// NOTIFIES CSS ERRORS
function errorAlert(error){
  notify.onError({title: "SCSS Error", message: "Check your terminal", sound: "Sosumi"})(error); //Error Notification
  console.log(error.toString());//Prints Error to Console
  this.emit("end"); //End function
};

// POST CSS PLUGINS
var processors = [
    lost(),
    cssnext(),
];

return gulp.src('app/styles/main.scss')
  .pipe(plumber({errorHandler: errorAlert}))
  // .pipe(sourcemaps.init())
  .pipe(sass())
  .pipe(postcss(processors))
  .pipe(rucksack())
  .pipe(csslint())
  .pipe(nano())
  // .pipe(sourcemaps.write())
  .pipe(gulp.dest('dist/styles/'))
  .pipe(bsync.stream());
});

//MINIFY CSS
gulp.task('minstyles', () => {

  // POST CSS PLUGINS
    var processors = [
        lost(),
        cssnext(),
    ];

  return gulp.src('app/styles/main.scss')
  .pipe(sass())
  .pipe(postcss(processors))
  .pipe(rucksack())
  .pipe(nano())
  .pipe(gulp.dest('dist/styles/main.min.css'))
});

//MOVE IMAGES TO DIST FOLDER FOR DEV ONLY (Default Task)
gulp.task('img', () => {
  return gulp.src('app/images/**/*')
  .pipe(gulp.dest('dist/images'));
});

//MOVE ASSETS TO DIST FOLDER FOR DEV ONLY (Default Task)
gulp.task('assets', () => {
  return gulp.src('app/assets/**/*')
  .pipe(gulp.dest('dist/assets'));
});

//IMAGE OPTIMIZATION
gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [
          {removeViewBox: false},
          {cleanupIDs: false}
      ],
      use: [pngquant()]
    }))
    .pipe(gulp.dest('dist/images'));
});

// HTML
gulp.task('html', () => {
  return gulp.src('app/*.html')
  .pipe(newer('dist'))
  .pipe(gulp.dest('dist'))
  .pipe(bsync.stream());
});

//NUNJUCKS TEMPLATES
gulp.task('nunjucks', () => {
  //nunjucksRender.nunjucks.configure(['app/templates/']);

  // Gets .html and .nunjucks files in pages
  return gulp.src('app/pages/**/*.+(html|nunjucks)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['app/templates/'] // String or Array
    }))
  // output files in app folder
  .pipe(gulp.dest('dist'))
  .pipe(bsync.stream());
});

//BROWSER-SYNC SERVER
gulp.task('server', (done) => {
  bsync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist', 'app'],
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });
  done();
});

//DEFAULT TASK
gulp.task('default', gulp.series('clean', 'nunjucks', 'img', 'assets',
  gulp.parallel('html', 'styles', 'scripts', 'vendorScripts'), 'server', (done) => {
    gulp.watch(['app/pages/**/*.html', 'app/templates/**/*.html'],     gulp.parallel('nunjucks'));
    gulp.watch('app/styles/**/*.scss',                                 gulp.parallel('styles'));
    gulp.watch('app/**/*.html',                                        gulp.parallel('html'));
    gulp.watch('app/scripts/**.js',                                    gulp.parallel('scripts'));
  done();
}));
