# Web Starter Kit

The *Web Starter Kit* is a collection/curation of my favorite libraries, frameworks, fonts, plugins, etc., for developing websites and web apps. It is powered by Gulp and SASS. All of the .scss files are @import inside of assets/sass/main.scss. The contents are commented out and located within assets/templates/_partials, simply uncomment the files you want to use. The majority of the contents also includes cdn hosted sources for production.

## Contents:

- [Animate.CSS](https://daneden.github.io/animate.css/)

- [Bootstrap](http://getbootstrap.com/)

- [Bourbon](http://bourbon.io/)

- [Breakpoint](http://breakpoint-sass.com/)

- [Dummy Text](http://dummytext.com/)

- [Enquire.js](http://wicky.nillia.ms/enquire.js/)

- [Font-Awesome](http://fontawesome.io/)

- [Foundation](http://foundation.zurb.com/)

- [Foundation Icons](http://zurb.com/playground/foundation-icon-fonts-3)

- [HTML5 Boilerplate](https://html5boilerplate.com/)

- [HoverCSS](http://ianlunn.github.io/Hover/)

- [Ion Icons](http://ionicons.com/)

- [jQuery](https://jquery.com/)

- [Light Gallery](http://sachinchoolur.github.io/lightGallery/index.html)

- [Magic CSS](http://www.minimamente.com/example/magic_animations/)

- [Material Design Lite](http://www.getmdl.io/)

- [Modernizr](https://modernizr.com/)

- [Neat](http://neat.bourbon.io/)

- [Normalize CSS](https://necolas.github.io/normalize.css/)

- [Owl Carousel](http://owlgraphic.com/owlcarousel/)

- [Skrollr](http://prinzhorn.github.io/skrollr/)

- [Slick Carousel](http://kenwheeler.github.io/slick/)

- [Susy](http://susy.oddbird.net/)

- [Timber Framework](http://framework.thememountain.com/index.html)

- [TypeBase.css](http://devinhunt.github.io/typebase.css/)

- [Typeplate Starter Kit](http://typeplate.com/)

- [VueJS](http://vuejs.org/)

- [Wow.JS](http://mynameismatthieu.com/WOW/)


















